# Tech News App Server

This is the server that runs the Tech News "app" on code.org.  It is written in PHP, uses MySQL/MariaDB for storage, and is hosted on Debian with NGINX.

## Source Support
Right now, **the server only supports Hacker News**, but more news sources could be added in the future by creating additional files in the `/getnews/` directory.  If the data is inserted in the database using the same format as the Hacker News entries, no modifications to the api should be neccesary.

## Design
The server/client relationship is relatively simple, the server has a cron job that triggers a request to be made every ~1 minute to the `cron.php` file.  That file then runs all the files in the `/getnews` directory.  Each file in that directory gets the news from a different news source, and all are added to the database.

When the app launches, it makes an HTTP request to `/api/news.php` to get all the articles to be displayed on the homepage.  Once you click on an article, it goes to a new page making a request to `/api/article.php?id=[ID]` (it dynamically inserts the article ID), and parses/displays the resulting data.

## Usability/Viability
This is just a project, and isn't designed to be used, there are a lot of shortcomings (see the "To-Do" section).  There are a lot of missing features and if anything this should be treated as a proof of concept.

## To-Do
Some things to be improved given more time.
- Make a real mobile app in React Native, Dart, etc.
- Use a better language (something compiled rather than interpreted) for the server-side to improve speed
- Automatically clear old articles from database after certain age
- Switch to a better database engine (something more fault-tolerant) like CockroackDB or PostgreSQL
- Make a web version (could be a static site that just uses client-side XHR requests)

## File Paths
`index.php` - Homepage/error page for visitors of the server

`cron.php` - This file is sent a get request using cURL on a cron job for every minute and triggers all the news collection and storage by running all files in the `/getnews/` directory.

`/getnews/` - These pages get news from various sources and add them to the database
- `hackernews.php` - Gets news from Hacker News

`/api/` - These pages are for the app to access
- `news.php` - Responds with last 3 articles of current news
- `article.php?id=[ID]` - Responds with the article's information encoded in JSON with these items
    - NOT NULL - `name` (string) - The article's name - 5 Amazing Facts About Penguins
    - NOT NULL - `author` (string) - The author of the article - John Doe
    - NOT NULL - `link` (string) - Link to the article - https://www.npr.org/2020/10/29/929095979/gray-wolves-to-be-removed-from-endangered-species-list
    - NULL - `comments` (string) - Link to comments section - https://news.ycombinator.com/item?id=24942434
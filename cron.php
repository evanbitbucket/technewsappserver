<?php
// This file is triggered every minute via a cron job on the server and cURL.  It triggers the contents of the /getnews directory.

foreach (glob("getnews/*.php") as $filename) { // look through directory
    include $filename; // include file from directory
}